package com.btrco.cloud.guestservices;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/guests")
public class GuestWebServices {
    private final GuetsRepository guetsRepository;

    public GuestWebServices(GuetsRepository guetsRepository) {
        this.guetsRepository = guetsRepository;
    }

    @GetMapping
    public Iterable<Guest> getAllGuests(){
        return this.guetsRepository.findAll();
    }

    @GetMapping("/{id}")
    public Guest getGuest(@PathVariable("id") Long id ) {
        return this.guetsRepository.findById(id).get();
    }
}
