package com.btrco.cloud.guestservices;

import org.springframework.data.repository.CrudRepository;

public interface GuetsRepository extends CrudRepository<Guest, Long> {
}
